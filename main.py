def num_of_subsequences(text, sentence, text_length, sentence_length):
    if (text_length == 0 and sentence_length == 0) or sentence_length == 0:
        return 1
    if text_length == 0:
        return 0

    if text[text_length - 1] == sentence[sentence_length - 1]:
        return (num_of_subsequences(text, sentence, text_length - 1, sentence_length - 1) +
                num_of_subsequences(text, sentence, text_length - 1, sentence_length))
    else:
        return num_of_subsequences(text, sentence, text_length - 1, sentence_length)


if __name__ == '__main__':

    sentence_dict = {}

    num_of_test_cases = input("Enter number of test cases:  ")

    if num_of_test_cases.strip().isdigit():
        for x in range(int(num_of_test_cases)):
            sentence = input('Enter Sentence:  ')
            text = input('Enter Text:  ')
            sentence_dict[text] = sentence
    else:
        print("please enter valid number")
        exit()

    for x, y in sentence_dict.items():
        print(y + " ---> ", x + " ---> ", num_of_subsequences(x, y, len(x), len(y)))
